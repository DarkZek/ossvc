extern crate sdl2;

use sdl2::audio::{AudioCallback, AudioSpecDesired};
use sdl2::audio::AudioDevice;
use sdl2::AudioSubsystem;
use std::sync::mpsc;
use std::i16;
use std::ffi::CStr;
use std::sync::mpsc::Receiver;

pub struct AudioRecorder {
    capture_device: Option<AudioDevice<Recording>>
}

impl AudioRecorder {

    pub fn new() -> AudioRecorder {
        AudioRecorder {
            capture_device: None
        }
    }

    pub fn setup(&mut self, audio_subsystem: &AudioSubsystem, desired_spec: &AudioSpecDesired, capture_device: &str) -> Receiver<Vec<i16>> {
        let (done_sender, done_receiver) = mpsc::channel();

        //Create capture device
        self.capture_device = Option::from(audio_subsystem.open_capture(None, desired_spec, |_spec| {
            Recording {
                //spec.freq as usize * spec.channels as usize
                record_buffer: vec![0; 2048],
                pos: 0,
                done_sender
            }
        }).unwrap());

        println!("Create capture device...");

        return done_receiver;
    }

    pub fn start_recording(&mut self, audio_subsystem: &AudioSubsystem, desired_spec: &AudioSpecDesired, capture_device: &str) -> Receiver<Vec<i16>> {

        println!("Started recording audio from capture device '{}'...", capture_device);

        let callback = self.setup(audio_subsystem, desired_spec, capture_device);

        self.capture_device.as_ref().unwrap().resume();

        return callback;
    }

    pub fn resume_recording(self) {
        self.capture_device.unwrap().resume();
    }

    pub fn pause_recording(self) {
        self.capture_device.unwrap().pause();
    }

    pub fn stop_recording(self) {
        self.capture_device.unwrap().close_and_get_callback();
    }

    pub fn debug_info(&self) {
        println!("AudioDriver: {:?}", self.capture_device.as_ref().unwrap().subsystem().current_audio_driver());
        println!("{:?}", self.capture_device.as_ref().unwrap().spec());
    }
}

struct Recording {
    record_buffer: Vec<i16>,
    pos: usize,
    done_sender: mpsc::Sender<Vec<i16>>,
}

// Append the input of the callback to the record_buffer.
// When the record_buffer is full, send it to the main thread via done_sender.
impl AudioCallback for Recording {
    type Channel = i16;

    fn callback(&mut self, input: &mut [i16]) {
        for x in input {
            self.record_buffer[self.pos] = *x;
            self.pos += 1;
            if self.pos >= self.record_buffer.len() {
                self.done_sender.send(self.record_buffer.clone())
                    .expect("could not send record buffer");

                let record_buffer_length = self.record_buffer.len();

                self.record_buffer = vec![0; record_buffer_length];
                self.pos = 0;
            }
        }
    }
}

pub fn get_capture_device_name(device_id: i32) -> &'static str {
    unsafe {
        let ptr = sdl2_sys::SDL_GetAudioDeviceName(device_id, 1);

        let c_str: &CStr = CStr::from_ptr(ptr);

        let device_name = c_str.to_str().unwrap();

        return &device_name;
    }
}

pub fn get_playback_device_name(device_id: i32) -> &'static str {
    unsafe {
        let ptr = sdl2_sys::SDL_GetAudioDeviceName(device_id, 0);

        let c_str: &CStr = CStr::from_ptr(ptr);

        let device_name = c_str.to_str().unwrap();

        return &device_name;
    }
}