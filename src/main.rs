extern crate sdl2;

use std::env;
use std::process::Command;
use sdl2::audio::AudioSpecDesired;
use sdl2::messagebox::MessageBoxFlag;
use sdl2::messagebox::ButtonData;
use sdl2::messagebox::MessageBoxButtonFlag;
use sdl2::messagebox::ClickedButton;

mod capture_utils;
use capture_utils::AudioRecorder;

mod playback_utils;
use playback_utils::AudioPlayback;

fn main() -> Result<(), String> {

    first_setup();

    let args: Vec<_> = env::args().collect();

    let mut arg1 = "";
    if args.len() > 1 {
        arg1 = args[1].as_str();
    }

    match arg1 {
        "--info" => info(),
        "-i" => info(),
        "--help" => help(),
        "-h" => help(),
        "-d" => start(args[2].parse().expect("Invalid Device Number")),
        _ => start(0)
    }

    Ok(())
}

fn first_setup() {
    //Run xterm -hold -e "echo 'sudo apt-get install sdl2-dev sdl2-mixer-dev' && sudo apt-get install sdl2-dev sdl2-mixer-dev"

    let buttons = vec![ButtonData{
        flags: (MessageBoxButtonFlag::NOTHING),
        button_id: 0,
        text: "Cancel",
    },ButtonData{
        flags: (MessageBoxButtonFlag::NOTHING),
        button_id: 1,
        text: "Install",
    }];

    let result = sdl2::messagebox::show_message_box(MessageBoxFlag::all(), &buttons, "Required Dependencies", "Dependencies need to be installed to use OSSVC", None, None).unwrap();

    match result {
        ClickedButton::CloseButton => {
            std::process::exit(0);
            return;
        },
        ClickedButton::CustomButton(data) => {
            if data.button_id == 0 {
                std::process::exit(0);
                return;
            }
        }
    }

    let output = Command::new("x-terminal-emulator").args(&["-e", "sudo apt-get install sdl2-dev sdl2-mixer-dev"])
        .status().expect("failed to execute process");

    if !output.success() {
        //Error!
        sdl2::messagebox::show_message_box(MessageBoxFlag::ERROR, &[], "A Error Has Occured", "Failed to install dependencies", None, None);
        std::process::exit(1);
    }
}

fn start(audio_device: i32) {
    let sdl_context = sdl2::init().unwrap();
    let audio_subsystem = sdl_context.audio().unwrap();
    let mut audio_recorder: AudioRecorder = AudioRecorder::new();
    let mut audio_player: AudioPlayback = AudioPlayback::new();

    let desired_spec = AudioSpecDesired {
        freq: None,
        channels: None,
        samples: None
    };

    let capture_device_name = capture_utils::get_capture_device_name(audio_device);
    let playback_device_name = capture_utils::get_playback_device_name(audio_device);

    println!("Capture: {} Playback: {}", capture_device_name, playback_device_name);

    let done_receiver = audio_recorder.start_recording(&audio_subsystem, &desired_spec, capture_device_name);

    let recorded_vec = done_receiver.recv().map_err(|e| e.to_string()).unwrap();

    audio_player.play(&audio_subsystem, &desired_spec, recorded_vec, playback_device_name);

    let mut iterations = 1000;

    while iterations > 0 {
        let recorded_vec = done_receiver.recv().map_err(|e| e.to_string()).unwrap();

        audio_player.play_new(recorded_vec);

        iterations -= 1;
    }

    println!("Exiting Application...");

    audio_recorder.stop_recording();
    audio_player.stop_playing();
}

fn info() {

    println!("Debug Info:");

    let sdl_context = sdl2::init().unwrap();
    let audio_subsystem = sdl_context.audio().unwrap();
    let mut audio_recorder: AudioRecorder = AudioRecorder::new();

    let desired_spec = AudioSpecDesired {
        freq: None,
        channels: None,
        samples: None
    };


    let capture_device_name = capture_utils::get_capture_device_name(0);
    audio_recorder.setup(&audio_subsystem, &desired_spec, capture_device_name);

    audio_recorder.debug_info();
}

fn help() {
    println!("OSSVC 0.01 ( https://gitlab.com/darkzek/OSSVC )");
    println!("Usage: ossvc [options]");
    println!();
    println!("Options taking time assume seconds.");
    println!("-h, --help                Shows this page");
    println!("-i, --info                Shows audio info of the machine");
}