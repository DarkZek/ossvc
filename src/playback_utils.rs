use sdl2::audio::{AudioCallback, AudioSpecDesired};
use sdl2::audio::AudioDevice;
use sdl2::AudioSubsystem;
use std::i16;


struct SoundPlayback {
    data: Vec<i16>,
    pos: usize,
}

pub struct AudioPlayback {
    playback_device: Option<AudioDevice<SoundPlayback>>
}

impl AudioPlayback {

    pub fn new() -> AudioPlayback {
        AudioPlayback {
            playback_device: None

        }
    }

    pub fn setup(&mut self, audio_subsystem: &AudioSubsystem, desired_spec: &AudioSpecDesired, recorded_vec: Vec<i16>, device_id: &str) {
        self.playback_device = Option::from(audio_subsystem.open_playback(None, desired_spec, |_spec| {
            SoundPlayback {
                data: recorded_vec,
                pos: 0,
            }
        }).unwrap());
    }

    pub fn play(&mut self, audio_subsystem: &AudioSubsystem, desired_spec: &AudioSpecDesired, recorded_vec: Vec<i16>, device_id: &str) {
        println!("Playing Back Audio...");

        self.setup(audio_subsystem, desired_spec, recorded_vec, device_id);

        // Start playback
        &self.playback_device.as_ref().unwrap().resume();
    }

    pub fn play_new(&mut self, recorded_vec: Vec<i16>) {
        {
            let mut lock = self.playback_device.as_mut().unwrap().lock();

            lock.data = recorded_vec.clone();
        }

        self.playback_device.as_ref().unwrap().resume();
    }

    pub fn resume_playing(&self) {
        self.playback_device.as_ref().unwrap().resume();
    }

    pub fn pause_playing(&self) {
        self.playback_device.as_ref().unwrap().pause();
    }

    pub fn stop_playing(self) {
        self.playback_device.unwrap().close_and_get_callback();
    }
}

impl AudioCallback for SoundPlayback {
    type Channel = i16;

    fn callback(&mut self, out: &mut [i16]) {
        for dst in out.iter_mut() {
            *dst = *self.data.get(self.pos).unwrap_or(&0);
            self.pos += 1;

            if self.pos == 2048 {
                self.pos = 0;
            }
        }
    }
}